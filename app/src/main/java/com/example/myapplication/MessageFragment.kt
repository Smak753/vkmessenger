package com.example.myapplication


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.vk.sdk.api.VKApiConst
import com.vk.sdk.api.VKParameters
import com.vk.sdk.api.VKRequest
import com.vk.sdk.api.VKResponse
import com.vk.sdk.api.model.VKApiMessage
import kotlinx.android.synthetic.main.fragment_message.*
import org.json.JSONArray
import org.json.JSONException


class MessageFragment : Fragment() {
lateinit var recView:RecyclerView
    companion object {
        fun newInstance(id: Int,name:String): android.support.v4.app.Fragment {
            val bundle = Bundle()
            bundle.putString("name",name)
            bundle.putInt("Id", id)
            val result = MessageFragment()
            result.arguments = bundle
            return result
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataChanged(activity!!.applicationContext)
        send_message.setOnClickListener ({
            var request = VKRequest("messages.send", VKParameters.from(VKApiConst.USER_ID, arguments!!.getInt("Id")
                    , VKApiConst.MESSAGE, input_message.text.toString()))
            request.executeWithListener(object : VKRequest.VKRequestListener() {
                override fun onComplete(response: VKResponse?) {
                    super.onComplete(response)
                     DownloadData.instance.takeDialogs()
                    Toast.makeText(context!!.applicationContext, "Сообщение отправлено", Toast.LENGTH_LONG).show()
                    dataChanged(activity!!.applicationContext)
                    input_message.setText("")
                }

            })
        })
    }

    fun dataChanged(context:Context) {
        var request = VKRequest("messages.getHistory", VKParameters.from(VKApiConst.USER_ID, arguments!!.getInt("Id")))
        request.executeWithListener(object : VKRequest.VKRequestListener() {
            override fun onComplete(response: VKResponse?) {
                super.onComplete(response)
                DownloadData.instance.takeDialogs()
                    var adapter = MessageAdapter(DownloadData.instance.getHistoryUserId(arguments!!.getInt("id")),arguments!!.getString("name"))
                    recView = message
                    var layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL, true)
                    recView.layoutManager = layoutManager
                    recView.adapter = adapter
                    recView.adapter.notifyDataSetChanged()
            }
        })

    }
}
