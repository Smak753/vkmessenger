package com.example.myapplication


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_dialog_message.*

class DialogFragment : Fragment() {
    companion object {
        fun newInstance():Fragment{
            val result = DialogFragment()
            return result
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var adapter = DialogAdapter(DownloadData.instance.getFullData())
        var recView = message_fragment
        recView.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        recView.adapter = adapter
        recView.adapter.notifyDataSetChanged()
    }
}// Required empty public constructor
