package com.example.myapplication

import android.util.Log
import com.vk.sdk.api.*
import com.vk.sdk.api.model.VKApiGetDialogResponse
import com.vk.sdk.api.model.VKApiMessage
import com.vk.sdk.api.model.VKApiUser
import com.vk.sdk.api.model.VKList
import org.json.JSONArray


class DownloadData private constructor(){

    private object Holder{val INSTANCE = DownloadData()}
    var DialogName = ArrayList<String>()
    var Id = ArrayList<Int>()
    var temp = ArrayList<Message>()
    var DataTime = ArrayList<Long>()
    companion object {
        val instance:DownloadData by lazy { Holder.INSTANCE }
    }

    var dialog = ArrayList<Dialog>()
    var i:String = ""

    fun takeDialogs(){

        var request: VKRequest = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.COUNT,50, VKApiConst.FIELDS,"photo_100"))
        request.executeWithListener(object: VKRequest.VKRequestListener(){
            override fun onComplete(response: VKResponse?) {

                var test = response!!.parsedModel as VKApiGetDialogResponse
                for(t in test.items){
                    if(!t.message.title.isEmpty()){
                    }
                    else{
                        if(i==""){
                            i = t.message.user_id.toString() + ","
                            DialogName.add(t.message.body)
                            Id.add(t.message.user_id)
                            DataTime.add(t.message.date)
                        }
                        else{
                            i = i+","+t.message.user_id
                            DialogName.add(t.message.body)
                            Id.add(t.message.user_id)
                            DataTime.add(t.message.date)
                        }

                    }
                }
                completData()
            }
        })
    }

    fun completData():ArrayList<Dialog>{
        var request: VKRequest = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS,i, VKApiConst.FIELDS,"photo_100"))
        request.executeWithListener(object : VKRequest.VKRequestListener(){
            override fun onComplete(response: VKResponse?) {
                var u = response!!.parsedModel as VKList<VKApiUser>
                var i=0
                for (t in u){
                    var temp = Dialog()
                    temp.text = DialogName[i]
                    temp.idUser = Id[i]
                    temp.first_name = t.first_name
                    temp.last_name= t.last_name
                    temp.photo = t.photo_100
                    temp.single = true
                    var date =DataTime[i].toString()
                    var time:String=date[0]+ date[1].toString() +":"+date[2]+date[3]
                    temp.date= time
                    Log.d(temp.first_name,t.id.toString())
                    dialog.add(temp)
                    i++
                }

            }
        })
        return dialog
    }

    fun getFullData():ArrayList<Dialog>{
        return dialog
    }

    fun getDownloadHistoryUserId(id:Int){
        var result = ArrayList<Message>()
        var request = VKRequest("messages.getHistory", VKParameters.from(VKApiConst.COUNT,50, VKApiConst.USER_ID,id))
        request.executeWithListener(object : VKRequest.VKRequestListener(){
            override fun onComplete(response: VKResponse?) {
                var array: JSONArray = response!!.json.getJSONObject("response").getJSONArray("items")
                var vkApiM = ArrayList<VKApiMessage>()
                for(i in 0..array.length()-1){
                    var mes = VKApiMessage(array.getJSONObject(i))
                    vkApiM.add(mes)
                }
                for(item in vkApiM){
                    if(item.out){
                        Log.d("OUT",item.body)
                        var date =item.date.toString()
                        var time:String=date[0]+ date[1].toString() +":"+date[2]+date[3]
                        result.add(Message(Message.Type.SENT,item.body,time))
                    }
                    else{
                        Log.d("IN",item.body)
                        var date =item.date.toString()
                        var time:String=date[0]+ date[1].toString() +":"+date[2]+date[3]
                        result.add(Message(Message.Type.RECEIVED,item.body,time))
                    }

                }
                temp = result
            }
        })
        Thread.sleep(1000)
    }

    fun getHistoryUserId(id:Int):ArrayList<Message>{
        getDownloadHistoryUserId(id)
        return temp
    }


}