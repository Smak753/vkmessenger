package com.example.myapplication

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.message_item.view.*

class MessageAdapter (var data:ArrayList<Message>,var name:String):RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {
        override fun getItemCount(): Int = data.size


        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MessageViewHolder
                = MessageViewHolder(LayoutInflater.from(parent?.context)
                .inflate(R.layout.message_item,parent,false))

        override fun onBindViewHolder(holder: MessageViewHolder?, position: Int) {
             when(data[position].getType()){
                Message.Type.SENT ->{
                    holder!!.messageText.gravity= Gravity.END
                    holder.userName.text = "Вы"
                    holder.messageText.text = data[position].getMessage()
                    holder.messageDate.text=data[position].getDate()
                }
                Message.Type.RECEIVED ->{
                    holder!!.messageText.gravity = Gravity.START
                    holder.messageText.text = data[position].getMessage()
                    holder.userName.text = name
                    holder.messageDate.text=data[position].getDate()
                }
            }
        }
        class MessageViewHolder(v: View): RecyclerView.ViewHolder(v) {
            var messageDate=v.message_date
            var userName= v.message_name
            var messageText= v.message_message
        }
}