package com.example.myapplication

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_item.view.*


class DialogAdapter(var data:ArrayList<Dialog>):RecyclerView.Adapter<DialogAdapter.DialogViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DialogViewHolder =
            DialogViewHolder(LayoutInflater.from(parent?.context)
                    .inflate(R.layout.dialog_item,parent,false))

    override fun onBindViewHolder(holder: DialogViewHolder?, position: Int) {
        if(data[position].photo!="")
            Picasso.with(holder!!.v.context)
                    .load(data[position].photo)
                    .into(holder.dialogImage)
        else
            holder!!.dialogImage.setImageResource(R.mipmap.ic_launcher)

        holder.dialogName.text = data[position].first_name
        if(data[position].text!="")
            holder.dialogText.text= data[position].text
        else
            holder.dialogText.text = ""
        holder.DialogDate.text = data[position].date
        holder.v.setOnClickListener {
            if (holder.v.context is MainActivity) {
                (holder.v.context as MainActivity).onOpenFragment(data[position].idUser
                        , data[position].first_name)
            }
        }
    }

    override fun getItemCount(): Int = data.size


    class DialogViewHolder(var v: View): RecyclerView.ViewHolder(v) {
        var dialogImage: ImageView = v.dialog_Image
        var dialogName: TextView = v.dialog_name
        var dialogText: TextView = v.dialog_message
        var DialogDate: TextView = v.dialog_date
    }

    interface onOpenHistoryCallback{
        fun onOpenFragment(id: Int,name:String)
    }
}