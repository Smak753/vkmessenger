package com.example.myapplication

open class Message(private val type: Type, private val message: String, private val date: String) {
    enum class Type {
        RECEIVED, SENT
    }

    fun getMessage():String{return message}
    fun getType():Type{return type}
    fun getDate():String{return date}
}