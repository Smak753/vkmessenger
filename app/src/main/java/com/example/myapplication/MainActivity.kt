package com.example.myapplication

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.vk.sdk.VKSdk
import com.vk.sdk.VKUIHelper
import com.vk.sdk.util.VKUtil
import com.vk.sdk.util.VKUtil.getCertificateFingerprint



class MainActivity : AppCompatActivity(),DialogAdapter.onOpenHistoryCallback {
    override fun onOpenFragment(id: Int,name:String) {
        DownloadData.instance.getDownloadHistoryUserId(id)
        if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.message_frame,MessageFragment.newInstance(id,name))
                    .addToBackStack("one")
                    .commit()
        }
        else
            supportFragmentManager.beginTransaction()
                    .replace(R.id.standart_frame,MessageFragment.newInstance(id,name))
                    .addToBackStack("one")
                    .commit()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        VKSdk.initialize(applicationContext)
        var orientation=resources.configuration.orientation
        if(orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.dialog_frame,DialogFragment.newInstance())
                    .commit()
        }
        else{
            supportFragmentManager.beginTransaction()
                    .replace(R.id.standart_frame,DialogFragment.newInstance())
                    .commit()
        }
    }
}
