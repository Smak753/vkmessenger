package com.example.myapplication

import android.util.Log
import com.vk.sdk.VKSdk

class Application: android.app.Application() {
        override fun onCreate() {
        super.onCreate()
        VKSdk.initialize(applicationContext)
        Log.d("SDK","ACTIVATED")
    }
}